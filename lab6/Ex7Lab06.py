#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo: Num determinado Estado, para transferências de veículos, o DETRAN cobra uma
taxa de 2,5% para carros fabricados antes de 2010 e uma taxa de 3,5% para os
fabricados de 2010 em diante, taxa esta incidindo sobre o valor de tabela do carro.
Escreva um programa lê o ano e o preço do carro e a seguir calcula e imprime a taxa a
ser paga.
'''

ano = int(input('Informe o ano do carro: '))
preco = float(input('Informe o preço do carro: '))

if ano < 2010:
	taxa = preco * 0.025
	print('A taxa a ser paga é de R$ %.2f' % taxa)
else:
	taxa = preco * 0.035
	print('A taxa a ser paga é de R$ %.2f' % taxa)