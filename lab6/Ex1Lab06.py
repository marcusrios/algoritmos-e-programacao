#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo: Faça um programa que leia um número e mostre uma mensagem indicando se este
número é positivo ou negativo.
'''

numero = int(input('Informe um número inteiro: '))

if numero < 0:
	print('O número informado é negativo')
elif numero > 0:
	print('O número informado é positivo')
else:
	print('O número informado é nulo(0)')