#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo: Faça um programa que receba um ano (quatro dígitos) e informe se é um ano
bissexto ou não. Pesquise quais as regras para o número ser bissexto.
'''

'''
Para um ano ser bissexto ele deve ser divisível por 4 e não pode ser divisível por 100
Um ano bissexto também pode ser divisível por 400
'''

ano = int(input('Informe um ano: '))

if ano % 4 == 0 and ano % 100 != 0:
	print('O ano informado é bissexto')
elif ano % 400 == 0:
	print('O ano informado é bissexto')
else:
	print('O ano informado não é bissexto')