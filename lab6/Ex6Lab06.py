#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo: Elabore um programa que leia do teclado o sexo de uma pessoa. Se o sexo digitado
for “M” ou “m” ou “F” ou “f”, escrever na tela “Sexo válido!”. Caso contrário, exibir
“Sexo inválido!”.
'''

sexo = input('Informe o sexo: ')

if sexo.upper() == 'M' or sexo.upper() == 'F':
	print('Sexo válido')
else:
	print('Sexo inválido')