#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo: Faça um programa que coloque dois nomes em ordem alfabética.
'''

primeiro_nome = input('Informe o primeiro nome: ')
segundo_nome = input('Informe o segundo nome: ')

if primeiro_nome < segundo_nome:
	print('A ordem alfabética é:', primeiro_nome,',', segundo_nome)
else:
	print('A ordem alfabética é:', segundo_nome,',', primeiro_nome)