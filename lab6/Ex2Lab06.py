#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo: Faça um programa que apresenta o maior de dois números lidos do usuário.
'''

numero1 = int(input('Informe o primeiro número inteiro: '))
numero2 = int(input('Informe o segundo número inteiro: '))

if numero1 < numero2:
	print('O maior número é', numero2)
else:
	print('O mario número é', numero1)