#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo:  Faça um programa que apresente se o número que o usuário digitou é divisível por 3 e por 5 ao mesmo tempo.
'''

numero = int(input('Informe um número: '))

if numero % 3 == 0 and numero % 5 == 0:
	print('O número informado é divisível por 3 e por 5 ao mesmo tempo')
else:
	print('O número informado não é divisível por 3 e 5')