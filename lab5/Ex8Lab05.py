#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo:  Desenvolva um programa que calcule (e imprima) o seno, cosseno e tangente de um número em radianos.
'''

import math

radiano = 0.5235987755982988 # 30 graus

print('O seno de 0.5235987755982988 radianos é:', math.sin(radiano))
print('O cosseno de 0.5235987755982988 radianos é:', math.cos(radiano))
print('A tangente de 0.5235987755982988 radianos é:', math.tan(radiano))