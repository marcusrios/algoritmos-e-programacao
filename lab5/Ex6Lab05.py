#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo: Desenvolva um programa que imprima a parte inteira e a parte de decimal de um número real
'''

import math

numero_real = 1.5
parte_inteira = math.trunc(1.5)
parte_fracionaria = numero_real - parte_inteira

print('A parte inteira do número real 1.5 é:', parte_inteira)
print('A parte fracionária do número real 1.5 é:', parte_fracionaria)