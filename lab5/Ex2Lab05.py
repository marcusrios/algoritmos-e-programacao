#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo: Desenvolva um programa que calcule a hipotenusa de um triangulo cujos catetos são 9cm e 4cm. (sem usar a função math.hypot() heim!).
'''

import math

catetos = 9 ** 2 + 4 ** 2
hipotenusa = math.sqrt(catetos)

print('A hipotenusa é de', hipotenusa,'cm')