#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo: Desenvolva um programa que calcule a área o volume do cilindro de raio 6cm e altura 5cm.
'''

import math

raio = 6
altura = 5

area = 2 * math.pi * raio * (raio + altura)
volume = math.pi * math.pow(raio, 2) * altura

print('A área do cilindro é', area,'cm2')
print('O volume do cilindro é', volume,'cm3')