#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo: Desenvolva um programa que arredonde e trunque um número real (apresente os resultados)
'''

print('O número real 3.3 arredondado é:', round(3.3))
print('O número real 8.8 arredondado é:', round(8.8))
