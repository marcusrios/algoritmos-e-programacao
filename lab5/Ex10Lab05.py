#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo: Faça um Programa que pergunte quanto você ganha por hora e o número de horas
trabalhadas no mês. Calcule e mostre o total do seu salário no referido mês, sabendo-se
que são descontados 11% para o Imposto de Renda, 8% para o INSS e 5% para o sindicato,
faça um programa que nos dê:
salário bruto.
quanto pagou ao INSS.
quanto pagou ao sindicato.
o salário líquido.
calcule os descontos e o salário líquido, conforme a tabela abaixo:
+ Salário Bruto : R$
- IR (11%) : R$
- INSS (8%) : R$
- Sindicato ( 5%) : R$
= Salário Liquido : R$
Obs.: Salário Bruto - Descontos = Salário Líquido.
'''

valor_hora = float(input('Informe o valor da sua hora: '))
horas_mes = float(input('Informe a quantidade de horas trabalhadas por mês: '))

salario_bruto = valor_hora * horas_mes
ir = salario_bruto * 0.11
inss = salario_bruto * 0.08
sindicato = salario_bruto * 0.05
descontos = ir + inss + sindicato
salario_liquido = salario_bruto - descontos

print('O salário bruto é de: R$', salario_bruto)
print('O valor pago de INSS foi de: R$', inss)
print('O valor pago ao sindicato foi de R$:', sindicato)
print('O salário líquido foi de: R$', salario_liquido)