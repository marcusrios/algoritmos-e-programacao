#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo: Desenvolva um programa que calcule e exiba na tela as raízes de 9, 16, 20, 25 e 42.
'''

import math

print('Raiz de 9 é:', math.sqrt(9))
print('Raiz de 16 é:', math.sqrt(16))
print('Raiz de 20 é:', math.sqrt(20))
print('Raiz de 25 é:', math.sqrt(25))
print('Raiz de 42 é:', math.sqrt(42))