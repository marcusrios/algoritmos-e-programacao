#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo: Desenvolva um programa que calcule a distância entre dois pontos no plano. Os valores dos pontos devem ser informados pelo usuário.
'''

ponto_a = float(input('Informe a posição do ponto A: '))
ponto_b = float(input('Informe a posição do ponto B: '))

distancia = ponto_b - ponto_a

print('A distância entre o ponto A e o ponto B é de:', distancia)