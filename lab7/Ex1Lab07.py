#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo: Esse é o jogo dos dados, muito usado em Las Vegas nos cassinos, aposte em um
número que seja o resultado da soma deles e ganhe o seu dinheiro. Crie duas variáveis
para representar os dados e uma para sua aposta, crie uma para armazenar o
resultado e faça a verificação.
'''

import random

aposta = int(input('Informe a sua aposta (Soma do resultado de dois dados): '))
soma_dados = random.randint(1,6) + random.randint(1,6)

if aposta == soma_dados:
	print('Você ganhou!')
else:
	print('Você perdeu. O número apostado foi', aposta,'e o resultado foi',soma_dados)