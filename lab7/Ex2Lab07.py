#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo: O Jogo do par ou ímpar é usado onde duas pessoas jogam geralmente para decidir
um impasse, cada um escolhe entre par ou ímpar e mostra o seu número, a soma
entre eles resulta em um número par ou ímpar e assim é decidido o vencedor. Aqui
faremos com a máquina, ela escolherá um número randômico entre 0 e 10 e você
escolherá o seu.
'''

import random

escolha = int(input('Informe um número entre 1 e 10: '))
sorteado = random.randint(1, 10)
escolha_maquina = random.randint(1, 10)

if escolha == sorteado:
	print('Você Ganhou!')
elif escolha_maquina == sorteado:
	print('Você perdeu. O número sorteado foi',sorteado)
else:
	print('Ninguém venceu. O número sorteado foi',sorteado)