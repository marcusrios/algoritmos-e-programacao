#!/usr/bin/env python3

'''
Autor: Marcus Rios
Objetivo: Um dos jogos sugeridos para crianças acima de 6 anos é o PEDRA, PAPEL E TESOURA
Como jogar:
Dois participantes ficam um de frente para o outro e, ao mesmo tempo, jogam uma
das mãos para frente representando um dos três símbolos: pedra (mão fechada), papel
(mão aberta) ou tesoura (dedos indicador e médio estendidos).
Para definir o vencedor segue-se a seguinte regra: pedra ‘quebra’ a tesoura; tesoura
‘corta’ o papel e papel ‘embrulha’ a pedra. Se ambas escolhem a mesma, há empate.
Este jogo também chama-se Joquempô, jo-quem-pô.
Sabendo como funciona o jogo crie uma variável para cada jogador que deve
armazenar a opção escolhida pela criança (Pedra, Papel ou Tesoura) e apresente o
resultado da jogada.
'''

import random

jogador1 = input('Primeiro jogador, selecione pedra, papel ou tesoura: ')
jogador2 = input('Segundo jogador, selecione pedra, papel ou tesoura: ')

if jogador1.upper() == 'PEDRA' and jogador2.upper() == 'TESOURA':
	print('O jogador1 ganhou!')
elif jogador1.upper() == 'TESOURA' and jogador2.upper() == 'PAPEL':
	print('O jogagor1 ganhou')
elif jogador1.upper() == 'PAPEL' and jogador2.upper() == 'PEDRA':
	print('O jogador1 ganhou')
elif jogador1.upper() == jogador2.upper():
	print('O jogo empatou!')
else:
	print('O jogador2 ganhou')